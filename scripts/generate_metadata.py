from tkinter import W
import click
from jinja2 import Template

@click.command()
@click.option("--template", type=click.File('rt'), required=True)
def main(template):
    t = Template(template.read())
    for i in range(1000):
        with open(f'./public/igonft/{i}.json', 'w') as f:
            f.write(t.render(tokenid=i))

if __name__ == '__main__':
    main()